#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <dirent.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/stat.h>
#include <pthread.h>

#define nThread 6

pthread_t tid[nThread];
pid_t child_id;

#define WHITESPACE 64
#define EQUALS 65
#define INVALID 66

static const unsigned char d[] = {
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 64, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 62, 66, 66, 66, 63, 52, 53,
    54, 55, 56, 57, 58, 59, 60, 61, 66, 66, 66, 65, 66, 66, 66, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
    10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 66, 66, 66, 66, 66, 66, 26, 27, 28,
    29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66};

int base64decode(char *in, size_t inLen, unsigned char *out, size_t *outLen){
    char *end = in + inLen;
    char iter = 0;
    uint32_t buf = 0;
    size_t len = 0;

    while (in < end){
        unsigned char c = d[*in++];

        switch (c){
        case WHITESPACE:
            continue; /* skip whitespace */
        case INVALID:
            return 1; /* invalid input, return error */
        case EQUALS:  /* pad character, end of data */
            in = end;
            continue;
        default:
            buf = buf << 6 | c;
            iter++; // increment the number of iteration
            /* If the buffer is full, split it into bytes */
            if (iter == 4){
                if ((len += 3) > *outLen)
                    return 1; /* buffer overflow */
                *(out++) = (buf >> 16) & 255;
                *(out++) = (buf >> 8) & 255;
                *(out++) = buf & 255;
                buf = 0;
                iter = 0;
            }
        }
    }

    if (iter == 3){
        if ((len += 2) > *outLen)
            return 1; /* buffer overflow */
        *(out++) = (buf >> 10) & 255;
        *(out++) = (buf >> 2) & 255;
    }
    else if (iter == 2){
        if (++len > *outLen)
            return 1; /* buffer overflow */
        *(out++) = (buf >> 4) & 255;
    }

    *outLen = len; /* modify to reflect the actual output size */
    return 0;
}

void downloadFiles(char *url, char *fileName){
    char download[100] = "https://docs.google.com/uc?export=download&id=";
    char tmp[100];
    strcpy(tmp, url);

    char *token = strtok(tmp, "/");
    while (token != NULL && !(strcmp(token, "d") == 0))
        token = strtok(NULL, "/");

    token = strtok(NULL, "/");
    strcat(download, token);

    char *argv[] = {"wget", "-q", "--no-check-certificate", download, "-O", fileName, NULL};
    execv("/usr/bin/wget", argv);
}

void unzip(char *fileName){
    char tmp[100];
    strcpy(tmp, fileName);
    char dirName[100];
    char *token = strtok(tmp, ".");
    strcpy(dirName, token);

    char *argv[] = {"unzip", fileName, "-d", dirName, NULL};
    execv("/usr/bin/unzip", argv);
}

void unzipWithPassword(char *fileName, char *password){
    char tmp[100];
    strcpy(tmp, fileName);
    char dirName[100];
    char *token = strtok(tmp, ".");
    strcpy(dirName, token);

    char *argv[] = {"unzip", "-P", password, fileName, "-d", dirName, NULL};
    execv("/usr/bin/unzip", argv);
}

void *downloadUnzipHandler(){
    char *url[2] = {"https://drive.google.com/file/d/1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1/view", "https://drive.google.com/file/d/1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt/view"};

    pthread_t id = pthread_self();
    if (pthread_equal(id, tid[0])){
        int status;
        child_id = fork();

        if (child_id == 0){
            pid_t child_id1 = fork();

            if (child_id1 == 0)
                downloadFiles(url[0], "music.zip");

            else{
                while ((wait(&status)) > 0);
                unzip("music.zip");
            }
        }
    }

    else if (pthread_equal(id, tid[1])){
        int status;
        child_id = fork();

        if (child_id == 0){
            pid_t child_id1 = fork();

            if (child_id1 == 0)
                downloadFiles(url[1], "quote.zip");

            else{
                while ((wait(&status)) > 0);
                unzip("quote.zip");
            }
        }
    }
    return NULL;
}

void decodeFiles(char *name){
    struct dirent **namelist;
    int n;

    n = scandir(name, &namelist, NULL, alphasort);
    int i = 0;
    if (n == -1)
        perror("scandir");

    for (int i = 0; i < n; i++){
        if (strcmp(namelist[i]->d_name, ".") != 0 && strcmp(namelist[i]->d_name, "..") != 0){
            char *decoded;
            char dirName[100];
            strcpy(dirName, name);
            strcat(dirName, "/");
            strcat(dirName, namelist[i]->d_name);
            free(namelist[i]);

            FILE *fp = fopen(dirName, "r");
            char line[100];
            while (fgets(line, 100, fp) != NULL){
                char *in = line;
                size_t inLen = strlen(in);
                size_t outLen = inLen;
                decoded = malloc(sizeof(char) * 32768);
                base64decode(in, inLen, (unsigned char *)decoded, &outLen);
            }
            fclose(fp);

            char txtDir[100];
            strcpy(txtDir, name);
            strcat(txtDir, "/");
            strcat(txtDir, name);
            strcat(txtDir, ".txt");

            fp = fopen(txtDir, "a");
            fprintf(fp, "%s\n", decoded);
            fclose(fp);
        }
    }
    free(namelist);
}

void *decodeHandler(){
    pthread_t id = pthread_self();
    if (pthread_equal(id, tid[2]))
        decodeFiles("music");
    
    else if (pthread_equal(id, tid[3]))
        decodeFiles("quote");

    return NULL;
}

void mkDir(char *name){
    char *argv[] = {"mkdir", "-p", name, NULL};
    execv("/usr/bin/mkdir", argv);
}

void moveFile(char *fileName){
    char src[100];
    strcpy(src, fileName);
    strcat(src, "/");
    strcat(src, fileName);
    strcat(src, ".txt");

    char dest[100];
    strcpy(dest, "hasil/");
    strcat(dest, fileName);
    strcat(dest, ".txt");

    char *argv[] = {"mv", src, dest, NULL};
    execv("/bin/mv", argv);
}

void *moveFileHandler(){
    pthread_t id = pthread_self();
    if (pthread_equal(id, tid[4])){
        int status;
        child_id = fork();

        if (child_id == 0){
            pid_t child_id1 = fork();

            if (child_id1 == 0)
                mkDir("hasil");

            else{
                while ((wait(&status)) > 0);
                moveFile("music");
            }
        }
    }
    else if (pthread_equal(id, tid[5])){
        int status;
        child_id = fork();

        if (child_id == 0){
            pid_t child_id1 = fork();

            if (child_id1 == 0)
                mkDir("hasil");

            else{
                while ((wait(&status)) > 0);
                moveFile("quote");
            }
        }
    }
    return NULL;
}

void zip(char *name, char *password){
    char fileName[100];
    strcpy(fileName, name);
    strcat(fileName, ".zip");

    char src[100];
    strcpy(src, name);
    strcat(src, "/*");

    char *argv[] = {"zip", "-P", password, "-r", "-j", fileName, name, NULL};
    execv("/usr/bin/zip", argv);
}

void deleteDir(char *name){
    char *argv[] = {"rm", "-rf", name, NULL};
    execv("/bin/rm", argv);
}

void addFile(char *name){
    FILE *fp = fopen(name, "w");
    fprintf(fp, "No\n");
    fclose(fp);
}

void *zipHandler(){
    char password[100] = "mihinomenest";
    char *user = getenv("USER");
    strcat(password, user);

    pthread_t id = pthread_self();
    if (pthread_equal(id, tid[6])){
        int status;
        child_id = fork();

        if (child_id == 0){
            pid_t child_id1 = fork();

            if (child_id1 == 0)
                zip("hasil", password);

            else{
                while ((wait(&status)) > 0);
                pid_t child_id2 = fork();

                if (child_id2 == 0)
                    deleteDir("hasil");

                else{
                    while ((wait(&status)) > 0);
                    pid_t child_id3 = fork();

                    if (child_id3 == 0)
                        unzipWithPassword("hasil.zip", password);

                    else{
                        while ((wait(&status)) > 0);
                        addFile("hasil/no.txt");
                        zip("hasil", password);
                    }
                }
            }
        }
    }
    return NULL;
}

int main(){
    int err;

    err = pthread_create(&tid[0], NULL, downloadUnzipHandler, NULL);
    if (err != 0)
        printf("\ncan't create thread :[%s]", strerror(err));
    else
        printf("\n create thread success %d\n", 1);

    err = pthread_create(&tid[1], NULL, downloadUnzipHandler, NULL);
    if (err != 0)
        printf("\ncan't create thread :[%s]", strerror(err));
    else
        printf("\n create thread success %d\n", 2);

    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);
    sleep(5);

    err = pthread_create(&tid[2], NULL, decodeHandler, NULL);
    if (err != 0)
        printf("\ncan't create thread :[%s]", strerror(err));
    else
        printf("\n create thread success %d\n", 3);

    err = pthread_create(&tid[3], NULL, decodeHandler, NULL);
    if (err != 0)
        printf("\ncan't create thread :[%s]", strerror(err));
    else
        printf("\n create thread success %d\n", 4);

    pthread_join(tid[2], NULL);
    pthread_join(tid[3], NULL);
    sleep(5);

    err = pthread_create(&tid[4], NULL, moveFileHandler, NULL);
    if (err != 0)
        printf("\ncan't create thread :[%s]", strerror(err));
    else
        printf("\n create thread success %d\n", 5);
    
    err = pthread_create(&tid[5], NULL, moveFileHandler, NULL);
    if (err != 0)
        printf("\ncan't create thread :[%s]", strerror(err));
    else
        printf("\n create thread success %d\n", 6);
    
    pthread_join(tid[4], NULL);
    pthread_join(tid[5], NULL);
    sleep(5);

    err = pthread_create(&tid[6], NULL, zipHandler, NULL);
    if (err != 0)
        printf("\ncan't create thread :[%s]", strerror(err));
    else
        printf("\n create thread success %d\n", 7);


    pthread_join(tid[6], NULL);
}