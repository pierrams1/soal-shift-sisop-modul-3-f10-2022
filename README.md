# SISOP Modul 3

## Nomer 1

Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.

## 1.A

Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread.

Dibuat thread 1 dan 2. Masing-masing thread berisi fungsi downloadUnzipHandler()

```c
void downloadFiles(char *url, char *fileName){
    char download[100] = "https://docs.google.com/uc?export=download&id=";
    char tmp[100];
    strcpy(tmp, url);

    char *token = strtok(tmp, "/");
    while (token != NULL && !(strcmp(token, "d") == 0))
        token = strtok(NULL, "/");

    token = strtok(NULL, "/");
    strcat(download, token);

    char *argv[] = {"wget", "-q", "--no-check-certificate", download, "-O", fileName, NULL};
    execv("/usr/bin/wget", argv);
}

void unzip(char *fileName){
    char tmp[100];
    strcpy(tmp, fileName);
    char dirName[100];
    char *token = strtok(tmp, ".");
    strcpy(dirName, token);

    char *argv[] = {"unzip", fileName, "-d", dirName, NULL};
    execv("/usr/bin/unzip", argv);
}

void *downloadUnzipHandler(){
    char *url[2] = {"https://drive.google.com/file/d/1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1/view", "https://drive.google.com/file/d/1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt/view"};

    pthread_t id = pthread_self();
    if (pthread_equal(id, tid[0])){
        int status;
        child_id = fork();

        if (child_id == 0){
            pid_t child_id1 = fork();

            if (child_id1 == 0)
                downloadFiles(url[0], "music.zip");

            else{
                while ((wait(&status)) > 0);
                unzip("music.zip");
            }
        }
    }

    else if (pthread_equal(id, tid[1])){
        int status;
        child_id = fork();

        if (child_id == 0){
            pid_t child_id1 = fork();

            if (child_id1 == 0)
                downloadFiles(url[1], "quote.zip");

            else{
                while ((wait(&status)) > 0);
                unzip("quote.zip");
            }
        }
    }
    return NULL;
}
```

Jadi masing-masing thread menjalankan tugas untuk mendownload file lalu mengekstraknya

### 1.B

Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.

Dibuat thread 3 dan 4. Masing-masing thread berisi fungsi decodeHandler()

```c
#define WHITESPACE 64
#define EQUALS 65
#define INVALID 66

static const unsigned char d[] = {
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 64, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 62, 66, 66, 66, 63, 52, 53,
    54, 55, 56, 57, 58, 59, 60, 61, 66, 66, 66, 65, 66, 66, 66, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
    10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 66, 66, 66, 66, 66, 66, 26, 27, 28,
    29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66};

int base64decode(char *in, size_t inLen, unsigned char *out, size_t *outLen){
    char *end = in + inLen;
    char iter = 0;
    uint32_t buf = 0;
    size_t len = 0;

    while (in < end){
        unsigned char c = d[*in++];

        switch (c){
        case WHITESPACE:
            continue; /* skip whitespace */
        case INVALID:
            return 1; /* invalid input, return error */
        case EQUALS:  /* pad character, end of data */
            in = end;
            continue;
        default:
            buf = buf << 6 | c;
            iter++; // increment the number of iteration
            /* If the buffer is full, split it into bytes */
            if (iter == 4){
                if ((len += 3) > *outLen)
                    return 1; /* buffer overflow */
                *(out++) = (buf >> 16) & 255;
                *(out++) = (buf >> 8) & 255;
                *(out++) = buf & 255;
                buf = 0;
                iter = 0;
            }
        }
    }

    if (iter == 3){
        if ((len += 2) > *outLen)
            return 1; /* buffer overflow */
        *(out++) = (buf >> 10) & 255;
        *(out++) = (buf >> 2) & 255;
    }
    else if (iter == 2){
        if (++len > *outLen)
            return 1; /* buffer overflow */
        *(out++) = (buf >> 4) & 255;
    }

    *outLen = len; /* modify to reflect the actual output size */
    return 0;
}

void decodeFiles(char *name){
    DIR *dp;
    if ((dp = opendir(name)) != NULL){
        struct dirent **namelist;
        int n;

        n = scandir(name, &namelist, NULL, alphasort);
        int i = 0;
        if (n == -1)
            perror("scandir");

        for (int i = 0; i < n; i++){
            if (strcmp(namelist[i]->d_name, ".") != 0 && strcmp(namelist[i]->d_name, "..") != 0){
                char *decoded;
                char dirName[100];
                strcpy(dirName, name);
                strcat(dirName, "/");
                strcat(dirName, namelist[i]->d_name);
                free(namelist[i]);

                FILE *fp = fopen(dirName, "r");
                char line[100];
                while (fgets(line, 100, fp) != NULL){
                    char *in = line;
                    size_t inLen = strlen(in);
                    size_t outLen = inLen;
                    decoded = malloc(sizeof(char) * 32768);
                    base64decode(in, inLen, (unsigned char *)decoded, &outLen);
                }
                fclose(fp);

                char txtDir[100];
                strcpy(txtDir, name);
                strcat(txtDir, "/");
                strcat(txtDir, name);
                strcat(txtDir, ".txt");

                fp = fopen(txtDir, "a");
                fprintf(fp, "%s\n", decoded);
                fclose(fp);
            }
        }
        free(namelist);
        closedir(dp);
    }
}

void *decodeHandler(){
    pthread_t id = pthread_self();
    if (pthread_equal(id, tid[2]))
        decodeFiles("music");

    else if (pthread_equal(id, tid[3]))
        decodeFiles("quote");

    return NULL;
}
```

Jadi masing-masing thread bertugas untuk mengdecode isi file txt

### 1.C

Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.

Dibuat thread 5 dan 6. Masing-masing thread berisi fungsi moveFileHandler()

```c
void mkDir(char *name){
    char *argv[] = {"mkdir", "-p", name, NULL};
    execv("/usr/bin/mkdir", argv);
}

void moveFile(char *fileName){
    char src[100];
    strcpy(src, fileName);
    strcat(src, "/");
    strcat(src, fileName);
    strcat(src, ".txt");

    char dest[100];
    strcpy(dest, "hasil/");
    strcat(dest, fileName);
    strcat(dest, ".txt");

    char *argv[] = {"mv", src, dest, NULL};
    execv("/bin/mv", argv);
}

void *moveFileHandler(){
    pthread_t id = pthread_self();
    if (pthread_equal(id, tid[4])){
        int status;
        child_id = fork();

        if (child_id == 0){
            pid_t child_id1 = fork();

            if (child_id1 == 0)
                mkDir("hasil");

            else{
                while ((wait(&status)) > 0);
                moveFile("music");
            }
        }
    }
    else if (pthread_equal(id, tid[5])){
        int status;
        child_id = fork();

        if (child_id == 0){
            pid_t child_id1 = fork();

            if (child_id1 == 0)
                mkDir("hasil");

            else{
                while ((wait(&status)) > 0);
                moveFile("quote");
            }
        }
    }
    return NULL;
}
```

Jadi masing-masing thread bertugas untuk membuat directorynya terlebih dahulu lalu memindahkan filenya ke directory hasil

### 1.D

Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)

### 1.E

Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.

Dibuat thread 7. Thread ini berisi fungsi zipHandler()

```c
void unzipWithPassword(char *fileName, char *password){
    char tmp[100];
    strcpy(tmp, fileName);
    char dirName[100];
    char *token = strtok(tmp, ".");
    strcpy(dirName, token);

    char *argv[] = {"unzip", "-P", password, fileName, "-d", dirName, NULL};
    execv("/usr/bin/unzip", argv);
}

void zip(char *name, char *password){
    char fileName[100];
    strcpy(fileName, name);
    strcat(fileName, ".zip");

    char src[100];
    strcpy(src, name);
    strcat(src, "/*");

    char *argv[] = {"zip", "-P", password, "-r", "-j", fileName, name, NULL};
    execv("/usr/bin/zip", argv);
}

void deleteDir(char *name){
    char *argv[] = {"rm", "-rf", name, NULL};
    execv("/bin/rm", argv);
}

void addFile(char *name){
    FILE *fp = fopen(name, "w");
    fprintf(fp, "No\n");
    fclose(fp);
}

void *zipHandler(){
    char password[100] = "mihinomenest";
    char *user = getenv("USER");
    strcat(password, user);

    pthread_t id = pthread_self();
    if (pthread_equal(id, tid[6])){
        int status;
        child_id = fork();

        if (child_id == 0){
            pid_t child_id1 = fork();

            if (child_id1 == 0)
                zip("hasil", password);

            else{
                while ((wait(&status)) > 0);
                pid_t child_id2 = fork();

                if (child_id2 == 0)
                    deleteDir("hasil");

                else{
                    while ((wait(&status)) > 0);
                    pid_t child_id3 = fork();

                    if (child_id3 == 0)
                        unzipWithPassword("hasil.zip", password);

                    else{
                        while ((wait(&status)) > 0);
                        addFile("hasil/no.txt");
                        zip("hasil", password);
                    }
                }
            }
        }
    }
    return NULL;
}
```

Thread ini bertugas untuk men-zip-kan folder hasil dengan password, lalu dia akan menghapus folder hasil, kemudian akan di unzip, tambah file no.txt, lalu di-zip lagi

### 1.Hasil

![image](/uploads/c20654bb6ee6fab10a022c3a8096a1e0/image.png)
![image](/uploads/73c3b86bef1aee00f21257849c607024/image.png)
![image](/uploads/0061f73f39df37decbac8a56ae502601/image.png)
![image](/uploads/4f1c1a0f5a24a88247443e08b9021099/image.png)

## Nomer 2

Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge sederhana. Online judge sederhana akan dibuat dengan sistem client-server dengan beberapa kriteria sebagai berikut:

### 2.A

Pada saat client terhubung ke server, terdapat dua pilihan pertama yaitu register dan login. Jika memilih register, client akan diminta input id dan passwordnya untuk dikirimkan ke server. Data input akan disimpan ke file users.txt dengan format username:password. Jika client memilih login, server juga akan meminta client untuk input id dan passwordnya lalu server akan mencari data di users.txt yang sesuai dengan input client. Jika data yang sesuai ditemukan, maka client dapat login dan dapat menggunakan command-command yang ada pada sistem. Jika tidak maka server akan menolak login client. Username dan password memiliki kriteria sebagai berikut:
Username unique (tidak boleh ada user yang memiliki username yang sama)
Password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil
Format users.txt:

isi users.txt:
username:password
username2:password2

```bash
/* Server Side */

void auth_handler(int new_socket){
    char choice[sz10b];
    valread = read(new_socket , choice, sz10b);
    if (choice[0] == 'r'){
        regis_handler(new_socket);
    }else if (choice[0] == 'l'){
        login_handler(new_socket);
    }
}
```

Dilakuakan pemilihan request dari client yang kemudian diterima server.

```bash
/* Client Side */

void auth(){
    char choice[1];
    printf("type r for register, l for login\n");
    scanf("%s", choice);
    printf("Dont type anything, wait until another client disconnect!\n");
    write(sock, choice, sz10b);
    if (choice[0] == 'r'){
        regist();
    }else if (choice[0] == 'l'){
        login();
    }
}
```

Dilakuakan pemilihan request client yang akan dikirim ke server.

```bash
/* Server side */

void regis_handler(int new_socket){
    char line[sz10b], data_user[sz10b][sz]={};
    int idx=0;
    FILE* fp = fopen(users_file,"r");
	if(!fp) {
        printf("Unable to open %s\n", users_file);
    }else {
        printf("Username List:\n");
        while (fgets(line, sizeof(line), fp)) {
            char *u;
            const char s[2] = ":";
            u = strtok(line, s);
            printf("-%s\n", u);
            strcpy(data_user[idx], u);
            idx++;
        }
        fclose(fp);
    }
    char temp_buff[sz10b]={};
    sprintf(temp_buff, "%d", idx);
    write(new_socket, temp_buff, sz10b);
    write(new_socket, data_user, sizeof(data_user));

    char user[sz10b]={},
         pass[sz10b]={};

    valread = read(new_socket, user, sz10b);
    valread = read(new_socket, pass, sz10b);
    fp = fopen(users_file,"a");
    fprintf(fp, "%s:%s\n", user, pass);
    printf("User [%s] Registered\n", user);
    fclose(fp);
}
```

Pada server side diatas diambil nama nama username dari file `users.txt` kemudian dikirimkan ke client side untuk dicek jika terdapat client baru yang register dengan username yang sama dengan database user. Kemudian mengambil username dan password dari client untuk disimpan ke database server.

```bash
/* Client Side */

void regist(){
    bool p_num = true, p_up = true, p_low = true, u_=false;
    char c, user[sz]={}, pass[sz]={}, temp_buff[sz10b]={};
    char data_user[sz10b][sz]={};
    int idx, i;
    valread = read(sock, temp_buff, sz10b);
    idx = string_to_int(temp_buff);
    valread = read(sock, data_user, sizeof(data_user));
    printf("Server free, you may type now\n");
    printf("Username: ");
    while(!u_){
        scanf("%s", user);
        for (i = 0; i < idx; i++){
            if (!strcmp(data_user[i], user)){
                u_=true;
                break;
            }
        }
        if (u_){
            u_ = false;
            printf("Username already exist\nUsername: ");
        }else {
            u_ = true;
            getchar();
        }
    }

    printf("Password: ");
    while(p_num || p_low || p_up) {
        c = getchar();
        while (c > 31){
            if (c == 32) {c = getchar(); continue;}
            if (p_num == true && c>47 && c<58) p_num = false;
            if (p_up == true && c>64 && c<91) p_up = false;
            if (p_low == true && c>96 && c<123) p_low = false;
            strncat(pass, &c, 1);
            c = getchar();
        }
        if(p_num || p_low || p_up || strlen(pass) < 6){
            printf("\nPassword Incorrect, password must contain the following:\n");
            if (strlen(pass) < 6) printf("- Minimum 6 characters\n");
            if (p_num) printf("- A  number\n");
            if (p_low) printf("- A  lowercase letter\n");
            if (p_up) printf("- An uppercase letter\n");
            p_num = p_up = p_low = true;
            memset(pass, '\0', sz);
            printf("Password: ");
        }
    }
    write(sock, user, sz10b);
    write(sock, pass, sz10b);
}
```

Pada client side akan mengambil data user dari database server untuk sebagai pengecekan nilai unique untuk username. Untuk password terdapat pengecekan sesuai permintaan soal jika sudah sesuai maka username dan password akan dikirim ke server.

### 2.B

Sistem memiliki sebuah database pada server untuk menampung problem atau soal-soal yang ada pada online judge. Database ini bernama problems.tsv yang terdiri dari judul problem dan author problem (berupa username dari author), yang dipisah dengan \t. File otomatis dibuat saat server dijalankan.

```bash
/* Server Side */

void make_tsv(){
    FILE* fp;
    if (fp = fopen(database, "r")){
    }else{
        fp = fopen(database, "a");
        fprintf(fp, "Judul\tAuthor\n");
    }
    fclose(fp);
}

void *client_connect_handler(void *sock){
    ...
    make_tsv();
    ...
}
```

Pada `make_tsv()` dilakukan pengecekan file database terlebih dahulu, jika belum ada maka dibuat dan dilakukan pembuatan kolom. Jika sudah ada maka dibiarkan.

### 2.C

Client yang telah login, dapat memasukkan command yaitu ‘add’ yang berfungsi untuk menambahkan problem/soal baru pada sistem. Saat client menginputkan command tersebut, server akan meminta beberapa input yaitu:
Judul problem (unique, tidak boleh ada yang sama dengan problem lain)
Path file description.txt pada client (file ini berisi deskripsi atau penjelasan problem)
Path file input.txt pada client (file ini berguna sebagai input testcase untuk menyelesaikan problem)
Path file output.txt pada client (file ini berguna untuk melakukan pengecekan pada submission client terhadap problem)
Contoh:

| Client-side |
| ----------- |
| add         |

| Server-side               |
| ------------------------- |
| Judul problem:            |
| Filepath description.txt: |
| Filepath input.txt:       |
| Filepath output.txt:      |

| Client-side              |
| ------------------------ |
| judul-problem-1          |
| Client/description.txt-1 |
| Client/input.txt-1       |
| Client/output.txt-1      |

Seluruh file akan disimpan oleh server ke dalam folder dengan nama `<judul-problem>` yang di dalamnya terdapat file description.txt, input.txt dan output.txt. Penambahan problem oleh client juga akan mempengaruhi file problems.tsv.

```bash
/* Server Side */

int command_handler(int new_socket){
    ...
    valread = read(new_socket, com, sizeof(com));
    if (!strcmp(com, "add")){
        printf("User [%s] adding a problem\n", userlog);
        FILE* fp = fopen(database, "a");
        char question[4][sz] = {"Judul problem:", "Filepath description.txt:", "Filepath input.txt:", "Filepath output.txt:"};
        write2d(question, 4, new_socket);
        char serv_path[sz*2], client_path[sz*2], prob[sz]={};
        read(new_socket, client_path, sizeof(client_path));
        for(int i=0; i<4; i++){
            char buffer[sz10b]={}, src_path[sz10b]={}, dest_path[sz10b]={};
            valread = read(new_socket, buffer, sizeof(buffer));
            if (i == 0){
                struct stat st = {};
                while (stat(buffer, &st) != -1) {
                    write(new_socket, "error", sz);
                    valread = read(new_socket, buffer, sizeof(buffer));
                }
                if (stat(buffer, &st) == -1) write(new_socket, "success", sz);
                fprintf(fp, "%s\t%s\n", buffer, userlog);
                strcpy(prob, buffer);
                mkdir(buffer, 0700);
                chdir(buffer);
                if (getcwd(serv_path, 100*2) == NULL){
                    perror("error\n");
                }
            }else{
                strcat(dest_path, serv_path);
                strcat(dest_path, "/");
                strcat(dest_path, buffer);
                strcat(src_path, client_path);
                strcat(src_path, "/");
                strcat(src_path, buffer);
                download_file(src_path, dest_path);
            }
        }
        chdir("..");
        printf("Problem [%s] has been added by [%s]\n", prob, userlog);
        fclose(fp);
        return 1;
    }
    ...
}
```

Pada server akan mengambil command jika commandnya `add` maka pertanyaan akan dilemparkan ke client dan kemudian diambil jawabannya, setelah jawab diambil file file yang di upload client akan di masukkan ke dalam suatu problem dengan nama judul problem yang diinput client. Setelah itu nama dan author pembuat problem akan di ketik di file `problem.tsv`.

```bash
/* Client Side */

int command(){
    ...
    write(sock, com, sizeof(com));
    if (!strcmp(com, "add")){
        char question[4][sz]={0};
        char client_dir[sz*2];
        getcwd(client_dir, sz*2);
        read2d(question, 4);
        write(sock, client_dir, sizeof(client_dir));
        for(int i=0; i<4; i++){
            char in[sz]={}, path[sz]={};
            printf("%s ", question[i]);
            scanf("%s", in);
            write(sock, in, sizeof(in));
            if (i==0){
                char temp[sz]={};
                read(sock, temp, sz);
                while(!(strcmp(temp, "error"))){
                    printf("Problem exist!\n%s ", question[i]);
                    scanf("%s", in);
                    write(sock, in, sizeof(in));
                    read(sock, temp, sz);
                }
            }
        }
        printf("\n");
        return 1;
    }
    ...
}
```

Pada client akan memasukan jawaban sesuai pertanyaan dari server kemudian jawab dikirimkan ke server dengan kriteria nama problem tidak boleh sama dengan yang sudah ada di server.

### 2.D

Client yang telah login, dapat memasukkan command ‘see’ yang berguna untuk menampilkan seluruh judul problem yang ada beserta authornya(author merupakan username client yang menambahkan problem tersebut). Format yang akan ditampilkan oleh server adalah sebagai berikut:

| Format output              |
| -------------------------- |
| judul-problem-1 by author1 |
| judul-problem-2 by author2 |

```bash
/* Server Side */

int command_handler(int new_socket){
    ...
    valread = read(new_socket, com, sizeof(com));
    ...
    else if (!strcmp(com, "see")){
        printf("User [%s] see problem list\n", userlog);
        FILE* fp = fopen(database, "r");
        char author[sz10b][sz]={}, problem[sz10b][sz]={};
        fscanf(fp, "%s %s\n", problem[0], author[0]);
        int idx = 0;
        while(fscanf(fp, "%s\t%s", problem[idx], author[idx]) == 2){
            idx++;
        }
        writeInt(idx, sz, new_socket);
        for(int i=0; i<idx; i++){
            write(new_socket, problem[i], sz);
            write(new_socket, author[i], sz);
        }
        fclose(fp);
        return 1;
    }
    ...
}
```

Pada server akan membuka file `problem.tsv` dan membaca semua isinya kemudian datanya di lempar ke client.

```bash
/* Client Side */

int command(){
    ...
    write(sock, com, sizeof(com));
    ...
    else if (!strcmp(com, "see")){
        char author[sz], problem[sz], buffer[sz];
        printf("Problem list:\n");
        valread = read(sock, buffer, sz);
        int idx = string_to_int(buffer);
        for(int i=0; i<idx; i++){
            read(sock, problem, sz);
            read(sock, author, sz);
            printf("-> %s by %s\n", problem, author);
        }
        printf("\n");
        return 1;
    }
}
```

Pada client semua data yang di ambil dari server akan di print sesuai format yang diminta.

### 2.E

Client yang telah login, dapat memasukkan command ‘download <judul-problem>’ yang berguna untuk mendownload file description.txt dan input.txt yang berada pada folder pada server dengan nama yang sesuai dengan argumen kedua pada command yaitu <judul-problem>. Kedua file tersebut akan disimpan ke folder dengan nama <judul-problem> di client.

```bash
/* Server Side */

void download_file(char *src_path, char *dst_path){
    int src_fd, dst_fd, n, err;
    unsigned char buffer[sz10b*10];

    src_fd = open(src_path, O_RDONLY);
    dst_fd = open(dst_path, O_CREAT | O_WRONLY);

    while (1) {
        err = read(src_fd, buffer, sz10b*10);
        if (err == -1) {
            printf("Error reading file.\n");
            exit(1);
        }
        n = err;

        if (n == 0) break;

        err = write(dst_fd, buffer, n);
        if (err == -1) {
            printf("Error writing to file.\n");
            exit(1);
        }
    }
    close(src_fd);
    close(dst_fd);
}

int command_handler(int new_socket){
    ...
    valread = read(new_socket, com, sizeof(com));
    ...
    else if (!strcmp(com, "download")){
        printf("User [%s] download a problem\n", userlog);
        char problem[sz]={}, client_path[sz*2]={}, desc_path[sz*2]={}, in_path[sz*2]={}, serv_path[sz*2]={}, i_c_path[sz*2]={}, d_c_path[sz*2]={}, message[sz10b]={};
        FILE* fp;
        valread = read(new_socket, problem, sz);
        valread = read(new_socket, client_path, sz*2);
        getcwd(serv_path, sz*2);
        strcat(desc_path, serv_path);
        strcat(desc_path, "/");
        strcat(desc_path, problem);
        strcat(desc_path, "/");
        strcat(in_path, desc_path);
        strcat(desc_path, "description.txt");
        strcat(in_path, "input.txt");
        if((fp = fopen(in_path, "r")) == NULL){
            strcpy(message, "Problem doesn't exist!");
            write(new_socket, message, sz10b);
            return 1;
        }
        strcat(client_path, "/");
        strcat(client_path, problem);
        mkdir(client_path, 0700);
        strcat(i_c_path, client_path);
        strcat(i_c_path, "/input.txt");
        strcat(d_c_path, client_path);
        strcat(d_c_path, "/description.txt");
        // printf("%s-\n", in_path);
        // printf("%s-\n", desc_path);
        // printf("%s-\n", i_c_path);
        // printf("%s-\n", d_c_path);
        if((fp = fopen(i_c_path, "r")) != NULL){
            fclose(fp);
            strcpy(message, "Problem has been already downloaded!");
            write(new_socket, message, sz10b);
            return 1;
        }
        download_file(in_path, i_c_path);
        download_file(desc_path, d_c_path);
        strcpy(message, "Problem successfully downloaded!");
        write(new_socket, message, sz10b);
        printf("Problem [%s] successfully downloaded by [%s]\n", problem, userlog);
        return 1;
    }
    ...
}
```

Pada server ketika client memberikan request `download`, server akan mengambil path client kemudian dilakukan exception jika problem yang diminta ada atau tidak dan juga jika tenyata client telah memiliki problem. Pada `download_file` dilakukan pembacaan sampai size yang dikirim ialah 0 atau pembacaan sudah tidak ada lagi.

```bash
/* Client Side */

int command(){
    ...
    write(sock, com, sizeof(com));
    ...
    else if (!strcmp(com, "download")){
        char problem[sz]={}, path[sz*2]={}, message[sz10b]={};
        scanf("%s", problem);
        write(sock, problem, sz);
        getcwd(path, sz*2);
        write(sock, path, sz*2);
        valread = read(sock, message, sz10b);
        printf("%s\n\n", message);
        return 1;
    }
}
```

Pada client diambil pathnya kemudian dikirim ke server side. Setelah pemrosesan pada server akan dikirim feedback message berhasil / tidak nya mendownload problem.

### 2.F

Client yang telah login, dapat memasukkan command ‘submit <judul-problem> <path-file-output.txt>’. Command ini berguna untuk melakukan submit jawaban dari client terhadap problem tertentu. Algoritma yang dijalankan adalah client akan mengirimkan file output.txt nya melalui argumen ke 3 pada command, lalu server akan menerima dan membandingkan isi file output.txt yang telah dikirimkan oleh client dan output.txt yang ada pada folder dengan nama yang sesuai dengan argumen ke 2 pada command. Jika file yang dibandingkan sama, maka server akan mengirimkan pesan “AC” dan jika tidak maka server akan mengeluarkan pesan “WA”.

```bash
/* Server Side */

int command_handler(int new_socket){
    ...
    valread = read(new_socket, com, sizeof(com));
    ...
    else if (!strcmp(com, "submit")){
        printf("User [%s] submit a problem\n", userlog);
        char problem[sz]={}, path[sz*2]={}, curr_path[sz*2]={};
        valread = read(new_socket, problem, sz);
        valread = read(new_socket, path, sz*2);
        getcwd(curr_path, sz*2);
        strcat(curr_path, "/");
        strcat(curr_path, problem);
        strcat(curr_path, "/");
        strcat(curr_path, "output.txt");
        FILE* fp = fopen(curr_path,"r");
        if(!fp) {
            printf("Unable to open %s\n", curr_path);
            exit(0);
        }
        char serv_out[sz10b][sz10b]={}, cli_out[sz10b][sz10b]={}, line[sz10b]={};
        int idx_serv=0, idx_cli=0;
        while (fgets(line, sizeof(line), fp)) {
            char *u;
            u = strtok(line, "\n");
            strcpy(serv_out[idx_serv], u);
            idx_serv++;
        }fclose(fp);
        fp = fopen(path,"r");
        if(!fp) {
            printf("Unable to open %s\n", path);
            exit(0);
        }
        while (fgets(line, sizeof(line), fp)) {
            char *u;
            u = strtok(line, "\n");
            strcpy(cli_out[idx_cli], u);
            idx_cli++;
        }fclose(fp);
        char message[sz]={};
        if(idx_serv != idx_cli) {
            strcpy(message, "WA");
            write(new_socket, message, sz);
            return 1;
        }
        for (int i=0; i<idx_serv; i++){
            printf("-%s-%s-\n", serv_out[i], cli_out[i]);
            if(strcmp(serv_out[i], cli_out[i])){
                strcpy(message, "WA");
                write(new_socket, message, sz);
                return 1;
            }
        }
        strcpy(message, "AC");
        write(new_socket, message, sz);
        return 1;
    }
    ...
}
```

Pada server side akan diambil isi dari `output.txt` client dan server. Kemudian pengecekan pertama pada index yang merupakan baris dari file jika banyak barisnya sudah berbeda antara `output.txt` server dan client maka akan dikirimkan message `WA` kepada client. Kemudian jika banyak baris sudah sama, selanjutnya akan dilakukan pengecekan isi string perbaris. Jika ada 1 yang berbeda maka akan dikirimkan message `WA` kepada client. Namun jika ternyata isinya sama maka akan dikirimkan message `AC`.

```bash
/* Client Side */

int command(){
    ...
    write(sock, com, sizeof(com));
    ...
    else if (!strcmp(com, "submit")){
        char problem[sz]={}, path[sz*2]={}, curr_path[sz*2], message[sz];
        scanf("%s%s", problem, path);
        getcwd(curr_path, sz*2);
        write(sock, problem, sz);
        strcat(curr_path, "/");
        strcat(curr_path, path);
        write(sock, curr_path, sz*2);
        valread = read(sock, message, sz);
        printf("%s\n\n", message);
        return 1;
    }

}
```

Pada client side akan diambil path dari output yang disubmit kemudian dikirimkan ke server. Jika pemrosesan pada server telah selesai, akan dikirimkan feedback message dari server.

### 2.G

Server dapat menangani multiple-connection. Dimana jika terdapat 2 atau lebih client yang terhubung ke server, maka harus menunggu sampai client pertama keluar untuk bisa melakukan login dan mengakses aplikasinya.

```bash
/* Server Side */

int main(){
    ...
    while((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))){
        printf("Connection accepted\n");

        pthread_t sniffer_thread;
		int *new_sock = malloc(1);
		*new_sock = new_socket;

		if( pthread_create( &sniffer_thread , NULL ,  client_connect_handler , (void*) new_sock) < 0){
			perror("could not create thread");
			return 1;
		}

		pthread_join(sniffer_thread ,NULL);
		printf("\nDisconnected from client\n");
        printf("Waiting for client connections...\n");
    }
    ...
}
```

Untuk setiap koneksi akan diambil menggunakan `accept()` function. Untuk pemrosesan menggunakan thread dengan inisialisasi `pthread_create()` function, kemudian pada soal diminta untuk melakukan handle client satu per satu sehingga menggunakan `pthread_join()` function untuk menunggu hingga satu clinet / satu thread selesai.

### 2.Hasil

2.A dan 2.G

![image](/uploads/d813828b3371ae466344b014a5040760/image.png)
![image](/uploads/fd32df02fb1f955e35ba4550732c04ee/image.png)

2.B
![image](/uploads/444f889cc9a72d9c8703f782d58a29d6/image.png)

2.C
![image](/uploads/2232b4f9790cec0c9912a8225e0d2343/image.png)

2.D
![image](/uploads/3269884048267afc72ee17f6eb212a94/image.png)

2.E
![image](/uploads/daf82b6d882b3d2fc6a3a6b69d83bbf1/image.png)

2.F
![image](/uploads/1da56713154259b85a912e1cffbb4b84/image.png)

## Kendala

1. Kurang bisa mengatur jalannya thread. Misalnya ingin menjalankan thread 3 dan 4 hanya ketika thread 1 dan 2 selesai dijalankan. Akhirnya untuk nomor 1 diakalin menggunakan sleep untuk menunda thread selanjutnya dijalankan dan menunggu thread sebelumnya selesai sepenuhnya.
2. Sempat error saat proses decode. Hasil akhirnya selalu ada character aneh atau tidak jelas. Untungnya ketemu cara fixnya dengan cara mengalokasikan memory yang cukup besar di variable string decoded `decoded = malloc(sizeof(char) * 32768)` ![messageImage_1649406015936](/uploads/abbcb96361681726359cb966b9fe47cd/messageImage_1649406015936.jpeg)
3. Pernah error saat pengiriman antara server dan client menggunakan write dan read. Ternyata, ketika mengirim suatu data ke socket, melakukan pembacaan harus sesuai dengan size pengiriman sehingga tidak ada data yang tertinggal maupun terpotong.

### 3

Nami adalah seorang pengoleksi harta karun handal. Karena Nami memiliki waktu luang, Nami pun mencoba merapikan harta karun yang dimilikinya berdasarkan jenis/tipe/kategori/ekstensi harta karunnya. Setelah harta karunnya berhasil dikategorikan, Nami pun mengirimkan harta karun tersebut ke kampung halamannya.
Contoh jika program pengkategorian dijalankan

Untuk 3A 3B dapat diselesaikan dengan menggunakan fork untuk bagian working directorynya pada file soal3.c lalu akan dicek apakah ada ekstensi yang dimana nanti akan dikategorikan. Untuk listFilesRecursively digunakan untuk menyusun file sesuai dengan ekstensinya.

### 3A,B

```bash

void* handler(void *arg)
{
    
    char things[200];
    strcpy(things,arg);

	unsigned long i=0;
	pthread_t id=pthread_self();
	int iter, index=0;
    char *arr2[50];
    char namaFile[200];
    char argCopy[100];

    //ngambil namafile beserta eksistensi
    char *token1 = strtok(things, "/");
    while (token1 != NULL) {
            arr2[index++] = token1;
            token1 = strtok(NULL, "/");
    }
    strcpy(namaFile,arr2[index-1]);

    //cek filenya termasuk dalam folder apa
    char *token = strchr(namaFile, '.');
    if (token == NULL) {
        strcat(argCopy, "Unknown");
    }
    else if (namaFile[0] == '.') {
        strcat(argCopy, "Hidden");
    }
    else {
        strcpy(argCopy, token+1);
        for (int i = 0; argCopy[i]; i++) {
            argCopy[i] = tolower(argCopy[i]);
        }
    }

    char source[1000], target[1000], dirToBeCreate[140];
    char *unhide, unhidden[200];
    strcpy(source, arg);
    if (sinyal == 1) {
        sprintf(target, "/home/pier/shift3/hartakarun/%s/%s", argCopy, namaFile);
        sprintf(dirToBeCreate, "/home/pier/shift3/hartakarun/%s/", argCopy);
        mkdir(dirToBeCreate, 0750);   
    } else if (sinyal == 2 || sinyal == 3) {
        if (namaFile[0] == '.') {
            namaFile[0] = '-';
        }
        sprintf(target, "%s/%s", argCopy, namaFile);
        sprintf(dirToBeCreate, "%s/", argCopy);
        mkdir(dirToBeCreate, 0750);
    }

    //pindah file
    if (rename(source,target) == 0) {
        printf("Berhasil Dikategorikan\n");
    } else printf("Gagal dikategorikan :(\n");

    return NULL;
}

void listFilesRecursively(char *basePath)
{
	char path[256]={};
	struct dirent *dp;
	DIR *dir = opendir(basePath);

	if (!dir)
	return;

	while ((dp = readdir(dir)) != NULL)
	{
		if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
		{
			if (dp->d_type == DT_REG)
			{
				strcpy(tanda[x], basePath);
				strcat(tanda[x], dp->d_name);
				x++;
			}
			else
			{
				strcpy(path, basePath);
				strcat(path, dp->d_name);
				strcat(path, "/");
				listFilesRecursively(path);
			}
		}
	}
	closedir(dir);
}


int main(int argc, char *argv[]) {
    int i=0,j=0;

    pid_t child_a, child_b, child_c, child_d;

    child_a = fork();
    // make shift3 dir
    if(child_a == 0)  {
        execl("/bin/mkdir", "mkdir_shift3", "-p","/home/pier/shift3", NULL);
    }

    waitpid(child_a, NULL, 0);
    child_b = fork();
    // make hartakarun dir
    if(child_b == 0) {
        execl("/bin/mkdir", "mkdir_hartakarun", "-p", "/home/pier/shift3/hartakarun", NULL);
    }

    waitpid(child_a, NULL, 0);
    waitpid(child_b, NULL, 0);
    child_c = fork();
    // unzip hartakarun.zip
    if(child_c == 0 && child_a > 0 && child_b > 0) {
        // change working dir to Downloads
        if(chdir("/home/pier/Downloads") < 0) perror("Error chdir to Downloads!\n");

        execl("/bin/unzip", "unzip_hartakarun.zip", "-q", "hartakarun.zip", "-d", "/home/pier/shift3/hartakarun", NULL);
    }

    waitpid(child_a, NULL, 0);
    waitpid(child_b, NULL, 0);
    waitpid(child_c, NULL, 0);
    child_d = fork();
    // make dir for each category
    // remove unrelated folders
    if(child_d == 0 && child_c > 0 && child_b > 0 && child_a > 0){
        if(chdir("/home/pier/shift3/hartakarun") < 0) perror("Error chdir to hartakarun!\n");

        sinyal = 3;
	    int err;
	    listFilesRecursively(path);

	    for (i=0; i<x; i++){
		    err=pthread_create(&(thd[i]),NULL,&handler,(void *) tanda[i]);
		    
            if(err!=0) return 0;
	    }
	    for (i=0; i<x; i++) pthread_join(thd[i],NULL);
    }   
    return 0; 
}
```

### 3C
Supaya proses kategori bisa berjalan cepat maka 1 file dioperasikan oleh 1 thread oleh fungsi berikut

```bash
sinyal = 3;
    int err;
    listFilesRecursively(path);

    for (i=0; i<x; i++){
	    err=pthread_create(&(thd[i]),NULL,&handler,(void *) tanda[i]);
	    
        if(err!=0) return 0;
    }
    for (i=0; i<x; i++) pthread_join(thd[i],NULL);
```

### 3D,E
Untuk no 3D dan 3E client harus bisa mengirimkan zip hartakarun ke server apabila di send tapi sebelum itu kita zip dahulu yang kemudian akan dicek dengan strcmp apakah user menginput send hartakarun.zip, jika iya maka akan dikirim ke tempat dimana server berada.

```bash
void* zip_file() {
    pid_t child_id;
    int status1;
        
    child_id = fork();
    if (child_id == 0) {
        printf("haloo");
        char *argv[] = {"zip", "-q", "-r", "hartakarun.zip", "/home/rafli/shift3/hartakarun", NULL};
        execv("/usr/bin/zip", argv);
    } else {
        while (wait(&status1) > 0);
    }
}


 char cmd[100];
  scanf("%[^\n]s", cmd);
  if(strcmp(cmd, "send hartakarun.zip") != 0){
	  puts("invalid command, exitting program");
	  return 0;
  }
  zip_file();

```

### Hasil
![image](/uploads/1wuLbsy5BgZ4JRjGvVjFow2FnPxVSdVWD/hasil2.JPG)
